<?php

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

/**
 * Roundcube single-sign-on (SSO) login to external mailaccount management via account link.
 *
 * @author Philip Iezzi <philip@onlime.ch>
 * @copyright Copyright (c) Onlime GmbH, https://www.onlime.ch
 */
class jwt_sso extends rcube_plugin
{
    /**
     * @var string
     */
    private $key;

    private $rc;

    public function init()
    {
        $this->load_config();

        $this->rc  = rcmail::get_instance();
        $this->key = $this->rc->config->get('jwt_sso_key');

        if ($this->rc->action == 'sso') {
            $user = $this->rc->user;

            if ($user) {
                if ($this->rc->config->get('jwt_sso_defaultdomain') ==  $user->get_username('domain')) {
                    $username = $user->get_username('local');
                } else {
                    $username = $user->get_username();
                }
                
                // workaround for expired session
                if (!$username) {
                    rcube::write_log('jwt-sso', sprintf(
                        'ERROR: Unknown user [%s] - not redirected to SSO-URL!',
                        $_SERVER['REMOTE_ADDR']
                    ));
                    $this->rc->output->show_message('Looks like your session has expired. Please login again.', 'error');
                    return false;
                }
                
                $ssoUrl = $this->getSsoUrl($username);
                
                rcube::write_log('jwt-sso', sprintf('User %s [%s]; SSO-URL: %s',
                    $username,
                    $_SERVER['REMOTE_ADDR'],
                    $ssoUrl
                ));
                
                header('Location: ' . $ssoUrl);
                exit;
	        }
        }
    }

    /**
     * Create and get JWT token.
     *
     * @param string $username
     * @return string
     * @throws Exception
     */
    private function getToken(string $username): string
    {
        $issuer = $this->rc->config->get('jwt_sso_issuer');
        $role   = $this->rc->config->get('jwt_sso_role');

        // workaround to set microseconds to zero, see:
        // https://github.com/lcobucci/jwt/issues/229#issuecomment-381999943
        $now = new DateTimeImmutable('@' . time());
        $exp = $now->modify('+' . $this->rc->config->get('jwt_sso_ttl') . ' minute');

        $jwtConfig = Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::base64Encoded($this->key)
        );
        $token = $jwtConfig->builder()
            ->issuedBy($issuer) // Configures the issuer (iss claim)
            ->relatedTo($username) // Configures the subject (sub claim)
            ->withClaim('identity', $username) // optional (some services use this instead of sub claim)
            ->withClaim('role', $role) // user role (private claim)
            ->withClaim('client_ip', @$_SERVER['REMOTE_ADDR']) // the client's IP (private claim)
            ->issuedAt($now) // Configures the time that the token was issue (iat claim)
            ->canOnlyBeUsedAfter($now) // Configures the time that the token can be used (nbf claim)
            ->expiresAt($exp) // Configures the expiration time of the token (exp claim)
            ->getToken($jwtConfig->signer(), $jwtConfig->signingKey());

        return $token->toString();
    }

    /**
     * Get SSO URL.
     *
     * @param string $username
     * @return string
     * @throws Exception
     */
    private function getSsoUrl(string $username): string
    {
        return sprintf(
            $this->rc->config->get('jwt_sso_url'),
            urlencode($this->getToken($username))
        );
    }
}
