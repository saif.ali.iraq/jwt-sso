# Roundcube JWT-SSO plugin

## Requirements

I only test this plugin with the following environments. Other setup may work with luck.

- PHP: >= `7.4`
- Roundcube: `1.4.x`

## Install

Install this Roundcube plugin with Composer (recommended). Go to your `ROUNDCUBE_HOME` (i.e., the root directory of your Roundcube installation) and run:

```bash
$ composer require onlime-roundcube/jwt-sso --update-no-dev
Do you want to activate the plugin jwt_sso? [Y|n]
```

Then change at least the following in `plugins/jwt_sso/config.in.php`:

```php
$config['jwt_sso_url'] = 'https://my.example.com/auth/sso-roundcube?token=%s';
$config['jwt_sso_key'] = '********************************';
```

> NOTE: The base64 encoded JWT secret key can be generated with e.g. `openssl`:
> 
> ```bash
> $ openssl rand -base64 32
> ```

If you did not select to activate the plugin directly from `composer require`, manually add `jwt_sso` to the plugins in your Roundcube config `config/config.inc.php`:

```php
$config['plugins'] = [
    // some other plugins...
    'jwt_sso', // <-- add this line
];
```

You can now integrate the SSO link in e.g. `skins/elastic/templates/includes/menu.html`:

```html
<a class="button-settings" id="rcmbtn-sso" href="?_action=sso" target="_blank">
    <span class="button-inner">Account</span>
</a>
```

## TODO

- [ ] Use `<roundcube:button ..>` instead of plain HTML in menu template
- [ ] Could the account button be directly added to taskmenu by this plugin?

## License

This plugin is released under the [GNU General Public License Version 3+][gpl].

## About

The project is hosted on [GitLab][project].
Please use the project's [issue tracker][issues] if you find bugs.

onlime-roundcube/jwt-sso was written by [Philip Iezzi](https://gitlab.com/piezzi) for [Onlime GmbH](https://www.onlime.ch).

[project]: https://gitlab.com/onlime/roundcube-plugins/jwt-sso
[issues]: https://gitlab.com/onlime/roundcube-plugins/jwt-sso/issues
[gpl]: https://www.gnu.org/licenses/gpl.html